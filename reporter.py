import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# var
report_list = [ "https://www.facebook.com/HongKongGoodNews/", "https://www.facebook.com/speakouthk/", "https://www.facebook.com/PeopleOfHK/"]
username = ""
password = ""

# Start Chrome
option = Options()
option.add_argument("--disable-infobars")
option.add_argument("start-maximized")
option.add_argument("--disable-extensions")
# option.add_argument("--headless")  # headless

# Pass the argument 1 to allow and 2 to block
option.add_experimental_option("prefs", {
    "profile.default_content_setting_values.notifications": 1
})
driver = webdriver.Chrome(chrome_options=option, executable_path='./chromedriver.exe')  # Optional argument, if not specified will search path.

# Login
driver.get("https://www.facebook.com")
driver.maximize_window()
driver.find_element_by_id('email').send_keys(username)
driver.find_element_by_id('pass').send_keys(password)
driver.find_element_by_id('loginbutton').click()
time.sleep(2)

# Report each page
while True:
    for page in report_list:
        driver.get(page)
        time.sleep(2)
        # Select opetion
        try:
            driver.find_element_by_xpath('//div[4]/button').click()
        except:
            driver.find_element_by_xpath('(//button[@value=\'1\'])[6]').click()
        time.sleep(3)
        # Click report
        driver.find_element_by_xpath('//li[7]/a/span/span').click()
        time.sleep(2)
        # Select option
        driver.find_element_by_xpath('//button[3]').click()
        time.sleep(2)
        driver.find_element_by_xpath('//div[3]/button[4]').click()
        time.sleep(3)
        # Sent report
        try:
            driver.find_element_by_xpath('(//button[@type=\'submit\'])[10]').click()
        except:
            driver.find_element_by_xpath('(// button[@ type=\'submit\'])[8]').click()
    time.sleep(5*60)
